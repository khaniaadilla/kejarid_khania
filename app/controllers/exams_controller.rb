class ExamsController < ApplicationController
    def new #untuk menampilkan form data baru
      @exams = Exams.new
    end
  
    def create #untuk memproses data baru yang dimasukan di form new
      #render plain: params.inspect cara mengecek apa isi params
      #title = params [:book][:title] cara mngecek balikan params
      exams = Exams.new(resource_params)
      Exams.save 
      flash[:notice] = 'Exam has been created'
      redirect_to exams_path
    end
  
    def edit #menampilkan data yang sudah disimpan di edit
      id = params[:id]
      @exams = Exams.find(id)
    end
  
    def update #melakuka prses ketika user mengedit data
      id = params[:id]
      @exams = Exams.find(params[:id])
      @exams.update(resource_params)
      flash[:notice] = 'Exam has been update'
        redirect_to exams_path(@exams)
      
    end
  
    def destroy #untuk menghapus data
       id = params[:id]
       exams = exams.find(params[:id])
       exams.destroy
       flash[:notice] = 'Exam has been delete'
       redirect_to exams_path
    end
  
    def index #menampilkan seluruh data yang ada di database
      @exams = exams.all
    end
  
    def show #menampilkan sebuah data secara detail
      id = params[:id]
      @exams = exams.find (id)
      # render plain : id
      # render plain : @book.title
  
    end
  private
  def resource_params
    params.require(:exams).permit(:title, :mapel, :duration, :nilai, :keterangan, :level, :student_id)
  end
  end
  