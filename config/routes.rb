Rails.application.routes.draw do
  # get 'students/create'
  # get 'students/show'
  # get 'students/update'
  # get 'students/index'
  # get 'students/destroy'
  resources :students
  resources :exams

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
