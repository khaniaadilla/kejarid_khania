# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Exams.create(title: 'PAS', mapel: 'matematika', duration: 30, nilai: 100, keterangan: 'Aktif', level: 'SMP', student_id: 1)
Exams.create(title: 'UTS', mapel: 'IPA', duration: 25, nilai: 50, keterangan: 'AKTIF', level: 'SMA', student_id: 2)
Exams.create(title: 'PAS', mapel: 'IPS', duration: 15, nilai: 80, keterangan: 'Aktif', level: 'SMP', student_id: 13)
